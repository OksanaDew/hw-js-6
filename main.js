//Метод forEach используется для перебора массива, он для каждого элемента массива вызывает функцию callback.
//Он ничего не возвращает, его используют только для перебора, как более «элегантный» вариант, чем обычный цикл for.

function filterBy(array, type) {
    let newArray = [];
    // array.forEach (!type, newArray, array);
    for (let i = 0; i < array.length; i++) {
        if (typeof(array[i]) !== type){
            newArray[newArray.length] = array[i];
        }
    }
    return newArray;
}

let arr = ['hello', 'world', 23, '23', null];

console.log(filterBy(arr, 'string'));

